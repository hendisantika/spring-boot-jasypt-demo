package com.hendisantika.springbootjasyptdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasypt-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/18
 * Time: 13.04
 * To change this template use File | Settings | File Templates.
 */
@Component
public class MyBean {

    @Value("${variable.secret-var}")
    private String mySecretVar;

    public void printVariable() {
        System.out.println("============================================");
        System.out.format("My secret variable is: %s\n", mySecretVar);
        System.out.println("============================================");
    }
}