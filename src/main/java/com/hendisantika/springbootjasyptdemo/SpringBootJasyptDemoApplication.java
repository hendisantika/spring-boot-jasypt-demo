package com.hendisantika.springbootjasyptdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringBootJasyptDemoApplication {

	public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringBootJasyptDemoApplication.class, args);
        MyBean bean = context.getBean(MyBean.class);
        bean.printVariable();
	}
}
