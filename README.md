# Spring Boot Jasypt Demo

This repository is intended for showing how to encrypt sensitive variable, such as password in properties file.

### Generate encrypt value

* Download jasypt from the link http://www.jasypt.org/download.html
* Extract the zip file and go to bin folder.
* You can see exe files under bin folder.
* Run the below command

```
./encrypt.sh input="bandung" password="supersecretz" algorithm="PBEWithMD5AndDES" verbose=true
./decrypt.sh input="HMooMwK7DCvB778KN1SLWXfTkmis1SpG" password="supersecretz" algorithm="PBEWithMD5AndDES" verbose=true
```